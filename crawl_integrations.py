#!/usr/bin/env python3

import gitlab
import json
import csv
import argparse
import requests
import yaml
import sys

def get_projects(gl, project_ids):
    projects = []
    for project_id in project_ids:
        project = gl.projects.get(project_id)
        projects.append(project.attributes)
    return projects

def get_group_projects(gl, groups):
    projects = []
    for topgroup in groups:
        group = gl.groups.get(topgroup)
        group_projects = group.projects.list(as_list=False, include_subgroups=True)
        for group_project in group_projects:
            projects.append(group_project.attributes)
    return projects

def get_instance_projects(gl):
    project_list = []
    projects = gl.projects.list(as_list=False)
    for project in projects:
        project_list.append(project.attributes)
    return project_list

def get_integration_settings(projects, integrations, api, api_url, headers):
    integration_settings = []
    for project in projects:
        project["integrations"] = []
        active_integrations = []
        try:
            all_integrations = requests.get("%s/projects/%s/%s" % (api_url, project["id"], api), headers=headers)
            if all_integrations.json():
                for active_integration in all_integrations.json():
                    active_integrations.append(active_integration["slug"])
            else:
                print("No integrations found: %s" % project["web_url"])
                continue
        except:
            print("Failed to retrieve integrations: %s" % project["web_url"], file=sys.stderr)
            continue

        for integration in integrations:
            if integration.lower() in active_integrations:
                try:
                    integration_setting = requests.get("%s/projects/%s/%s/%s" % (api_url, project["id"], api, integration.lower()), headers=headers)
                    if integration_setting.json():
                        project["integrations"].append(integration_setting.json())
                    else:
                        print("Failed to retrieve integration settings for %s: %s" % (integration.lower(), project["web_url"]), file=sys.stderr)
                except:
                    print("Failed to retrieve integration settings for %s: %s" % (integration.lower(), project["web_url"]), file=sys.stderr)
    return projects

def format_projects(projects):
    formatted_projects = []
    needed_fields = ["id","path_with_namespace","integrations"]
    for project in projects:
        if project["integrations"]:
            formatted_project = {}
            for field in needed_fields:
                formatted_project[field] = project[field]
            formatted_projects.append(formatted_project)
    return formatted_projects

parser = argparse.ArgumentParser(description='Create report for GitLab issues')
parser.add_argument('token', help='API token able to read the requested projects')
parser.add_argument('configfile', help='YML file that defines requested groups, projects and adoption parameters')
parser.add_argument('--gitlab', help='URL of the gitlab instance', default="https://gitlab.com/")

args = parser.parse_args()

gl = gitlab.Gitlab(args.gitlab, private_token=args.token)
gitlab = args.gitlab if args.gitlab.endswith("/") else args.gitlab + "/"
api_url = gitlab + "api/v4"
headers = {'PRIVATE-TOKEN': args.token}

configfile = args.configfile
groups = []
projects = []
integrations = []
api = "integrations"

with open(configfile, "r") as c:
    config = yaml.load(c, Loader=yaml.FullLoader)
    if "groups" in config:
        groups = config["groups"]
        projects = get_group_projects(gl, groups)
    else:
        if not "projects" in config:
            projects = get_instance_projects(gl)
        else:
            projects = get_projects(gl, config["projects"])

    if "integrations" in config:
        integrations = config["integrations"]
    else:
        print("No integrations specified, exiting")
        exit(1)

    if "api" in config:
        api = config["api"]
    else:
        print("Warning: No API specified, defaulting to 'integrations' (GitLab 14.4+)")

projects_with_integrations = get_integration_settings(projects, integrations, api, api_url, headers)
formatted_projects = format_projects(projects_with_integrations)

with open("project_integrations.json","w") as outfile:
    json.dump(formatted_projects, outfile, indent = 2)
