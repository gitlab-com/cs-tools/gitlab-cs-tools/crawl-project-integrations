# Crawl project integrations

Crawl integrations of all projects and export their properties.

The script returns a `json` file containing all projects that have the specified integrations configured as well as the integration properties.

## Usage

`python3 crawl_integrations.py $GIT_TOKEN $CONFIG_FILE --gitlab $INSTANCE`

**Parameters:**

* $GIT_TOKEN: A personal access token with read_api permissions, able to see access all projects that you want to report on
* $CONFIG_FILE: A config.yml file stating which projects to report on and which integrations to crawl
* $INSTANCE: Optional instance url, defaults to `https://gitlab.com`

When running with GitLab CI, make sure to configure the token as masked, protected CI variable.

## Configuration

Specify in the config file which groups or projects to crawl. Defining no groups or projects will default to all projects accessible with your personal access token. **Note: In case of an admin token, that will be all projects in the instance.**

Also specify which API to use via the `api` keyword. For GitLab versions before 14.4, that would be:

`api: "services"`

For GitLab versions after 14.4, that would be:

`api: "integrations"`

Finally, specify which integrations to retrieve the properties for. You can find a list [here](https://docs.gitlab.com/ee/api/integrations.html).

You can find an example file in the repo: [config.yml](./config.yml).

## DISCLAIMER

This script is provided **for educational purposes only**. It is not supported by GitLab. You can create an issue or contribute via MR if you encounter any problems. This script only performs `GET` calls to the API, so it only reads data.